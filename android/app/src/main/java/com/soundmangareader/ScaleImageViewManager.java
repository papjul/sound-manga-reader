package com.soundmangareader;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import androidx.annotation.Nullable;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

public class ScaleImageViewManager extends SimpleViewManager<SubsamplingScaleImageView> {
    public static final String REACT_CLASS = "ScaleImageView";

    private ThemedReactContext reactContext = null;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected SubsamplingScaleImageView createViewInstance(ThemedReactContext reactContext) {
        this.reactContext = reactContext;
        return new SubsamplingScaleImageView(reactContext);
    }

    @ReactProp(name = "uri")
    public void setUri(SubsamplingScaleImageView view, @Nullable String uri) {
        view.setMaxScale(10f); // Allow small images to be upscaled
        view.setImage(ImageSource.uri(uri));
    }
}
