package com.soundmangareader;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

public class ScaleImageModule extends ReactContextBaseJavaModule {
    ScaleImageModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
        return "ScaleImageModule";
    }
}
