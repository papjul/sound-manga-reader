import * as RNLocalize from 'react-native-localize';
import {I18n} from 'i18n-js';
import memoize from 'lodash.memoize';
import {I18nManager} from 'react-native'; // Use for caching/memoize for better performance

const translationGetters: {[key: string]: () => object} = {
  // lazy requires (metro bundler does not support symlinks)
  en: () => require('../translations/en.json'),
  fr: () => require('../translations/fr.json'),
  pt: () => require('../translations/pt.json'),
  'zh-Hans': () => require('../translations/zh-Hans.json'),
  'zh-Hant': () => require('../translations/zh-Hant.json'),
};

export const i18n = new I18n();

export const translate = memoize(
  (key, config?) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = () => {
  // fallback if no available language fits
  const fallback = {languageTag: 'en', isRTL: false};

  const {languageTag, isRTL} =
    RNLocalize.findBestLanguageTag(Object.keys(translationGetters)) || fallback;

  // clear translation cache
  if (translate.cache.clear !== undefined) {
    translate.cache.clear();
  }

  // update layout direction
  I18nManager.forceRTL(isRTL);

  // set i18n-js config
  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
};
