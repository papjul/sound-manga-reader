import AsyncStorage from '@react-native-async-storage/async-storage';
import {Dirs, FileSystem} from 'react-native-file-access';
import {Linking, ToastAndroid} from 'react-native';

export const SDCardDir = Dirs.SDCardDir + '/';

export const getSoundMangaPackDirectory = async (fullDir: boolean) => {
  try {
    const soundMangaPackDirectory = await AsyncStorage.getItem(
      '@SMR_directory',
    );
    if (soundMangaPackDirectory !== null) {
      const exists = await FileSystem.exists(
        SDCardDir + soundMangaPackDirectory,
      );
      if (exists) {
        return (fullDir ? SDCardDir : '') + soundMangaPackDirectory;
      } else {
        return null;
      }
    } else {
      return null;
    }
  } catch (e) {
    console.log(e);
    // error reading value
    return null;
  }
};

export const grantManageExternalStoragePermission = async () => {
  return Linking.sendIntent(
    'android.settings.MANAGE_ALL_FILES_ACCESS_PERMISSION',
    [
      {
        key: 'android.provider.extra.APP_PACKAGE',
        value: 'com.soundmangareader',
      },
    ],
  );
};
