import React, {ReactNode, useEffect, useState} from 'react';
import {useParams} from 'react-router-native';
import _, {capitalize} from 'lodash';
import {
  ActivityIndicator,
  Button,
  Dialog,
  List,
  Portal,
  Text,
} from 'react-native-paper';
import {FileSystem} from 'react-native-file-access';
import {BackHandler, Linking, StyleSheet} from 'react-native';
import Layout from '../components/Layout';
import {translate} from '../../utils/localize';
import DeviceInfo from 'react-native-device-info';
import {useNavigate} from 'react-router-native';
import {ScriptManifest} from '../../utils/file';
import {SDCardDir} from '../../utils/preferences';

const s = StyleSheet.create({
  bottomMargin: {
    marginBottom: 10,
  },
  link: {
    color: '#ff9800',
  },
});

interface MangaViewProps {
  packsDir: string;
}
const MangaView = ({packsDir}: MangaViewProps) => {
  const navigate = useNavigate();

  const [visible, setVisible] = useState(false);
  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const goToMainScreen = () => {
    navigate('/', {replace: true});
    return true;
  };

  useEffect(() => {
    const backPressListener = BackHandler.addEventListener(
      'hardwareBackPress',
      goToMainScreen,
    );
    return () => {
      backPressListener.remove();
    };
  }, []);

  const params = useParams();
  const manga = params.manga ? params.manga : '';

  const [script, setScript] = useState<ScriptManifest | null>(null);
  const [loadMessage, setLoadMessage] = useState<ReactNode>(
    <ActivityIndicator size="large" />,
  );

  useEffect(() => {
    FileSystem.readFile(SDCardDir + packsDir + '/' + manga + '/script.json')
      .then(result => {
        setLoadMessage(<ActivityIndicator size="large" />);
        setScript(JSON.parse(result));
      })
      .catch(err => {
        if (err.code === 'ENOENT') {
          setLoadMessage(
            <>
              <Text variant="bodyMedium" style={s.bottomMargin}>
                {manga + ' ' + translate('unavailable')}
              </Text>
            </>,
          );
        } else {
          setLoadMessage(translate('errorLoadingScript') + manga);
        }
        setScript(null);
        console.log(err.message, err.code);
      });
  }, [manga]);

  return script ? (
    <Layout
      title={script ? script.title : manga}
      goBack={() => goToMainScreen()}
      moreInfo={() => showDialog()}>
      <Text variant="titleLarge">{script.title}</Text>
      {script.story !== undefined && script.story !== '' && (
        <Text variant="bodyMedium">
          {translate('story')}
          {translate('colon')} {script.story}
        </Text>
      )}
      {script.art !== undefined && script.art !== '' && (
        <Text variant="bodyMedium">
          {translate('art')}
          {translate('colon')} {script.art}
        </Text>
      )}
      {script.soundScript !== undefined && script.soundScript !== '' && (
        <Text variant="bodyMedium">
          {translate('soundScript')}
          {translate('colon')} {script.soundScript}
        </Text>
      )}

      {Array.isArray(script.translations[script.lang]) &&
        script.translations[script.lang].length > 0 && (
          <>
            <Text variant="bodyMedium">
              {translate('translation')}
              {translate('colon')}
            </Text>
            {script.translations[script.lang].map(({name, url}) => (
              <React.Fragment key={'trans' + name}>
                {url ? (
                  <Text
                    variant="bodyMedium"
                    style={s.link}
                    onPress={() => Linking.openURL(url)}>
                    {name}
                  </Text>
                ) : (
                  <Text variant="bodyMedium">{name}</Text>
                )}
              </React.Fragment>
            ))}
          </>
        )}

      {!_.isEmpty(script.volumes) &&
        Object.values(script.volumes).map(chapters => {
          if (chapters) {
            return chapters.map(chapter => (
              <List.Item
                key={'link-' + manga + '-ch-' + chapter}
                title={translate('chapter') + ' ' + chapter}
                description={
                  script.chapters[chapter].title[script.lang] !== null &&
                  script.chapters[chapter].title[script.lang]
                }
                onPress={() => navigate('chapter/' + chapter, {replace: true})}
              />
            ));
          }
        })}

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>
            {capitalize(translate('version')) + ' ' + DeviceInfo.getVersion()}
          </Dialog.Title>
          <Dialog.Content>
            <Button
              onPress={() => navigate('integrity', {replace: false})}
              style={s.bottomMargin}
              mode="outlined">
              {translate('checkIntegrity')}
            </Button>
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={hideDialog}>{translate('done')}</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </Layout>
  ) : (
    <Layout title={manga} goBack={() => goToMainScreen()}>
      <Text variant="bodyMedium">{loadMessage}</Text>
    </Layout>
  );
};

export default MangaView;
