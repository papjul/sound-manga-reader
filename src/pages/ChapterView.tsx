import React, {ReactNode, useEffect, useState} from 'react';
import Reader from '../components/Reader';
import {useNavigate, useParams} from 'react-router-native';
import {FileSystem} from 'react-native-file-access';
import Layout from '../components/Layout';
import {ActivityIndicator, Text} from 'react-native-paper';
import {translate} from '../../utils/localize';
import {PagerItem, ScriptManifest} from '../../utils/file';
import {SDCardDir} from '../../utils/preferences';

interface ChapterViewProps {
  packsDir: string;
}
const ChapterView = ({packsDir}: ChapterViewProps) => {
  const params = useParams();
  const navigate = useNavigate();

  const [pagerItems, setPagerItems] = useState<PagerItem[]>([]);
  const [loadMessage, setLoadMessage] = useState<ReactNode>(
    <ActivityIndicator size="large" />,
  );

  const manga = params.manga ? params.manga : '';
  const chapter = params.chapter ? params.chapter : '';
  const [initialIndex, setInitialIndex] = useState<number | null>(null);
  const [title, setTitle] = useState(manga);

  useEffect(() => {
    FileSystem.readFile(SDCardDir + packsDir + '/' + manga + '/script.json')
      .then(result => {
        setLoadMessage(<ActivityIndicator size="large" />);

        const tmpScript: ScriptManifest = JSON.parse(result);
        let tmpPagerItems: PagerItem[] = [];
        let tmpIndex: number | null = null;
        Object.values(tmpScript.volumes).forEach(chapters => {
          if (chapters) {
            chapters.forEach(chapterIndex => {
              const pages = tmpScript.chapters[chapterIndex].pages;
              if (chapter === chapterIndex && tmpIndex === null) {
                tmpIndex = tmpPagerItems.length;
              }
              for (let i = 0; i < pages.length; ++i) {
                // TODO: Dirty
                pages[i].se.forEach(function (se, seIndex, theArray) {
                  // Do not switch to => function for "this" to work
                  theArray[seIndex] =
                    SDCardDir +
                    packsDir +
                    '/' +
                    tmpScript.soundDir +
                    '/se/' +
                    se +
                    '.ogg';
                });
                const tmpPagerItem: PagerItem = {
                  chapter: chapterIndex,
                  page: pages[i].page,
                  bgm: pages[i].bgm
                    ? SDCardDir +
                      packsDir +
                      '/' +
                      tmpScript.soundDir +
                      '/bgm/' +
                      pages[i].bgm +
                      '.ogg'
                    : null,
                  se: pages[i].se,
                  voice: pages[i].voice
                    ? SDCardDir +
                      packsDir +
                      '/' +
                      manga +
                      '/voice/ch-' +
                      chapterIndex +
                      '/' +
                      pages[i].page +
                      '.ogg'
                    : null,
                  uri:
                    SDCardDir +
                    packsDir +
                    '/' +
                    manga +
                    '/img/ch-' +
                    chapterIndex +
                    '/' +
                    pages[i].page +
                    '.jpg',
                };
                tmpPagerItems.push(tmpPagerItem);
              }
            });
          }
        });
        if (tmpIndex !== null) {
          setPagerItems(tmpPagerItems.slice().reverse());
          setInitialIndex(tmpPagerItems.length - tmpIndex - 1);
        }
        setTitle(tmpScript.title);
      })
      .catch(err => {
        if (err.code === 'ENOENT') {
          setLoadMessage(manga + ' ' + translate('unavailable'));
        } else {
          setLoadMessage(translate('errorLoadingScript') + manga);
        }
        setPagerItems([]);
        setInitialIndex(null);
        console.log(err.message, err.code);
      });
  }, [manga]);

  const goToMangaScreen = () => {
    navigate('/manga/' + manga, {replace: true});
    return true;
  };

  return pagerItems && initialIndex ? (
    <Reader
      title={title}
      pagerItems={pagerItems}
      manga={manga}
      initialIndex={initialIndex}
    />
  ) : (
    <Layout
      title={translate('chapter') + ' ' + chapter + ' | ' + manga}
      goBack={() => goToMangaScreen()}>
      <Text variant="bodyMedium">{loadMessage}</Text>
    </Layout>
  );
};

export default ChapterView;
