import React, {useEffect, useRef, useState} from 'react';
import {useNavigate, useParams} from 'react-router-native';
import _ from 'lodash';
import {Text} from 'react-native-paper';
import {FileStat, FileSystem} from 'react-native-file-access';
import {AppState, AppStateStatus, BackHandler} from 'react-native';
import Layout from '../components/Layout';
import {translate} from '../../utils/localize';
import {compareFiles, prettifyMusicFolderName} from '../../utils/file';
import MusicPlayLink from '../components/MusicPlayLink';
import {Player} from '@react-native-community/audio-toolkit';
import {SDCardDir} from '../../utils/preferences';

interface MusicBoxViewProps {
  packsDir: string;
}
const MusicBoxView = ({packsDir}: MusicBoxViewProps) => {
  const navigate = useNavigate();

  const [musics, setMusics] = useState<FileStat[]>([]);

  const bgmRef = useRef<Player>(null);
  const [bgm, setBgm] = useState(bgmRef.current);

  const loadBgm = (file: string) => {
    let tmpBgm = new Player(
      SDCardDir + packsDir + '/' + folder + '/bgm/' + file,
    );
    tmpBgm.looping = true;
    tmpBgm.volume = 0.8;
    tmpBgm.play();
    if (bgmRef.current) {
      bgmRef.current.destroy();
    }
    // @ts-ignore | FIXME: current should not be modified directly
    bgmRef.current = tmpBgm;
    setBgm(tmpBgm);
  };

  const destroyBgm = () => {
    if (bgmRef.current) {
      bgmRef.current.destroy();
      // @ts-ignore | FIXME: current should not be modified directly
      bgmRef.current = null;
      setBgm(bgmRef.current);
    }
  };

  const goToMainScreen = () => {
    navigate('/', {replace: true});
    return true;
  };

  /** APP STATE **/
  const appState = useRef(AppState.currentState);

  useEffect(() => {
    const appStateChangeListener = AppState.addEventListener(
      'change',
      _handleAppStateChange,
    );

    return () => {
      appStateChangeListener.remove();
    };
  }, []);

  const _handleAppStateChange = (nextAppState: AppStateStatus) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      destroyBgm();
    }

    appState.current = nextAppState;
  };

  useEffect(() => {
    const backPressListener = BackHandler.addEventListener(
      'hardwareBackPress',
      goToMainScreen,
    );
    return () => {
      backPressListener.remove();
    };
  }, []);

  useEffect(() => {
    return () => {
      destroyBgm();
    };
  }, []);

  const params = useParams();
  const folder = params.folder ? params.folder : '';

  useEffect(() => {
    FileSystem.statDir(SDCardDir + packsDir + '/' + folder + '/bgm/').then(
      result => {
        const musicFound = [];
        if (result !== undefined && result.length > 0) {
          for (let index in result) {
            const item = result[index];
            if (item.type === 'file' && item.filename.endsWith('.ogg')) {
              musicFound.push(item);
            }
          }
        }
        musicFound.sort(compareFiles);
        setMusics(musicFound);
      },
    );
  }, []);

  return (
    <Layout
      title={folder ? prettifyMusicFolderName(folder) : ''}
      goBack={() => goToMainScreen()}>
      <Text variant="titleLarge">{translate('bgmList')}</Text>

      {!_.isEmpty(musics) &&
        Object.values(musics).map(music => (
          <MusicPlayLink
            packsDir={packsDir}
            key={'music-' + music.filename}
            folder={folder}
            file={music.filename}
            onPress={loadBgm}
          />
        ))}
    </Layout>
  );
};

export default MusicBoxView;
