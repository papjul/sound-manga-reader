import {List} from 'react-native-paper';
import React, {useEffect, useState} from 'react';
// @ts-ignore | FIXME: Not supported
import MediaMeta from 'react-native-media-meta';
import {SDCardDir} from '../../utils/preferences';

interface MusicPlayLinkProps {
  packsDir: string;
  folder: string;
  file: string;
  onPress: any; // FIXME
}

const MusicPlayLink = ({
  packsDir,
  folder,
  file,
  onPress,
}: MusicPlayLinkProps) => {
  const [title, setTitle] = useState(file.replace('.ogg', ''));
  const [artist, setArtist] = useState(null);

  useEffect(() => {
    MediaMeta.get(SDCardDir + packsDir + '/' + folder + '/bgm/' + file)
      // @ts-ignore | FIXME
      .then(metadata => {
        if (metadata.title) {
          setTitle(metadata.title);
        } else {
          setTitle(file.replace('.ogg', ''));
        }
        if ('artist' in metadata) {
          setArtist(metadata.artist);
        }
      });
  }, [file]);

  return (
    <List.Item
      title={artist ? title + ' – ' + artist : title}
      onPress={() => onPress(file)}
    />
  );
};

export default MusicPlayLink;
