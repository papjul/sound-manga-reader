Version 20230528
* Material 3 theme
* Allow to choose the directory in which packs are stored (packs won't be deleted on uninstall)
* Packs will be updated on homepage when app is back to the foreground, to avoid force closing the app to refresh list
* Dependencies updated

Version 20220628
* Add a basic Music Box that can play BGM from sound packs

Version 20220617
* Fix order of listed manga

Version 20220615
* Umineko Manga Reader becomes Sound Manga Reader. The change of name reflects the ability to design sound packs with any manga. The package name was also changed so it can be installed in parallel of Umineko Manga Reader, however Umineko Manga Reader will no longer be updated, so it is suggested to uninstall it once you successfully upgraded to the new app.
* Manga are now dynamically found in the obb/ directory.
* !Breaking Change! The "common" directory (now called "sound directory") is now defined from each pack (to avoid conflicts with other manga). Older packs are incompatible. Only the script.json from each pack need to be downloaded again.
* !Breaking Change! Audio files in the Umineko sound pack were renamed to remove "umilse" and others hardcoded strings in app. The Umineko sound pack need to be fully downloaded again.
* Updated installation instructions
* Automatic light/dark theme
* Buttons are replaced with lists for manga list and chapter list
* Remove iOS and Windows related code (can be added back in case a developer can maintain it)
* Update dependencies

Version 20211010
* Fix an issue where chapters 24b and 24c1-3 from episode 8 were not accessible
* Update dependencies

Version 20210822
* Update dependencies

Version 20210227b
* Actually upscale image on devices with very large resolution this time

Version 20210227
* Upscale image on devices with very large resolution

Version 20210224
* Grammar fixes to English translation, thanks to @griffonmcmahon
* Fix: restore bold filenames and paths in Installation instructions

Version 20210223
* Fix regression of image quality on zoom on Android

Version 20210213
* Add support for Umineko Murasaki
* Bug fixed on pinch-to-zoom to prevent swiping while zooming
* UI improvements to the reader
* Add immersive mode (fullscreen) to the reader
* Move Installation instructions to a separate page
* Move Credits to a separate About page
* Show app version in About page
* Move Integrity button to More info button
* Add episode version in More info dialog

Version 20210205
* User experience improvements

Version 20210204
* Add support for Chinese simplified and traditional (thanks to Amber)
* Add support for title translations
* Add support for translation credits
* Fix an issue where music apps would scan all Umineko sounds, due to being in Android/media/
* All packs need to be redownloaded (old version is incompatible) and unzipped in Android/obb/com.uminekomangareader/

Version 20210201b
* Create Android/media/com.uminekomangareader/ folder on first start if it doesn't exist.

Version 20210201
* Add support for (upcoming) episode 8
* Move assets path from Android/data/com.uminekomangareader/ to Android/media/com.uminekomangareader/ to workaround Android 11 restrictions. You will need to move your existing files to the new location when upgrading, whether you are running Android 11 or not.
* Add credits section

Version 20210131
* Add ability to turn off BGM/SE/voice

Version 20210128
* Integrity check

Version 20210127
* Make the installation instructions easier to understand
* Remove APK download link from GitLab as it is wrongly detected as ZIP file

Version 20210126
- Huge performance improvements
- New icon
- Fix resuming audio after going to background

Version 1.0
- Initial version
